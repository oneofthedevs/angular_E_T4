import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'impurePipe',
  pure: false,
})
export class ImpurePipePipe implements PipeTransform {
  transform(value: any[]): any {
    for (let i in value) {
      if (value[i].toLowerCase() === 'orangutan') value[i] = '🦧';
      if (value[i].toLowerCase() === 'shark') value[i] = '🦈';
      if (value[i].toLowerCase() === 't-rex') value[i] = '🦖';
      if (value[i].toLowerCase() === 'fox') value[i] = '🦊';
      if (value[i].toLowerCase() === 'happyface') value[i] = '😃';
      if (value[i].toLowerCase() === 'burger') value[i] = '🍔';
      if (value[i].toLowerCase() === 'alligator') value[i] = '🐊';
    }
    return value;
  }
}
