import { Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { interval, Observable } from 'rxjs';
import { BoredApiService } from './services/boredApi/bored-api.service';
import { take } from 'rxjs/operators';

@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public displayTime: any;
  public apiData: any;
  private time: Observable<any> = interval(1000);
  private callMethod: Observable<any> = interval(2500);

  public arr: any[] = [
    'shark',
    'alligator',
    'burger',
    't-rex',
    'happyface',
    'orangutan',
  ];

  constructor(private _boredApi: BoredApiService) {}

  ngOnInit(): void {
    this.getLatestTime();
    this.callAPI();
    this.callMethodFunc();
  }

  private getLatestTime(): void {
    this.time.subscribe(() => (this.displayTime = new Date()));
  }

  private callAPI(): void {
    this._boredApi
      .callBoredApi()
      .pipe(untilDestroyed(this))
      .subscribe((res) => (this.apiData = res));
  }

  private addEmoji(): void {
    this.arr.push('fox');
  }

  private callMethodFunc(): void {
    this.callMethod
      .pipe(take(5), untilDestroyed(this))
      .subscribe(() => this.addEmoji());
  }
}
