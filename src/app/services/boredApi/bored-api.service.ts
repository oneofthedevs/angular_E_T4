import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BoredApiService {
  private baseURL: string = 'https://www.boredapi.com/api/activity';
  constructor(private _http: HttpClient) {}

  public callBoredApi(): Observable<any> {
    return this._http.get(`${this.baseURL}`);
  }
}
