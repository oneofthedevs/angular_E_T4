import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { interval, Observable } from 'rxjs';

@UntilDestroy({ checkProperties: true })
export class CommonService {
  public arr: any[] = [
    'shark',
    'alligator',
    'burger',
    't-rex',
    'happyface',
    'orangutan',
  ];
  private callMethod: Observable<any> = interval(2500);

  constructor() {
    this.callMethodFunc();
  }

  private addEmoji(): void {
    this.arr.push('fox');
  }

  private callMethodFunc(): void {
    this.callMethod.pipe(untilDestroyed(this)).subscribe(() => this.addEmoji());
  }
}
