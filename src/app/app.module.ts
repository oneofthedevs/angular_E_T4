import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PurePipePipe } from './pipes/purePipe/pure-pipe.pipe';
import { ImpurePipePipe } from './pipes/impurePipe/impure-pipe.pipe';
import { HttpClientModule } from '@angular/common/http';
import { PurePipeComponent } from './components/pure-pipe/pure-pipe.component';
import { ImpurePipeComponent } from './components/impure-pipe/impure-pipe.component';

@NgModule({
  declarations: [AppComponent, PurePipePipe, ImpurePipePipe, PurePipeComponent, ImpurePipeComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
