import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common/common.service';

@Component({
  selector: 'pure-pipe',
  templateUrl: './pure-pipe.component.html',
  styleUrls: ['./pure-pipe.component.scss'],
})
export class PurePipeComponent {
  public _common: CommonService;
  constructor() {
    this._common = new CommonService();
  }
}
