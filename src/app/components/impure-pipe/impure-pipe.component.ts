import { Component } from '@angular/core';
import { CommonService } from 'src/app/services/common/common.service';

@Component({
  selector: 'impure-pipe',
  templateUrl: './impure-pipe.component.html',
  styleUrls: ['./impure-pipe.component.scss'],
})
export class ImpurePipeComponent {
  public _common: CommonService;
  constructor() {
    this._common = new CommonService();
  }
}
